import 'package:flutter/material.dart';

//POPUP FLOTANTE DE MENSAJES DE ALERTA
Future<void> errorDialog(String message, context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Error', style: TextStyle(
          color: Colors.red
        )),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[Text(message)],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('CERRAR'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}


Future<void> successDialog(String message, context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('Ejecución exitosa', style: TextStyle(
          color: Colors.blue
        )),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[Text(message)],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('CERRAR'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
