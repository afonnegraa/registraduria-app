import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Credenciales {
  //CARGA EL CORREO DEL USUARIO POR MEDIO DEL DOCUMENTO
  Future<String> loginStart(
    String usuario,
    String contrasena
  ) async {
    var userEmail = await Firestore.instance.collection('funcionario').document(usuario).get().then(
      (DocumentSnapshot result) {
        if(!result.exists) {
          return 'error';
        }
        return result.data['correo'];
      }
    );
    print(userEmail);
    return await loginPostService(userEmail, contrasena); //SE EJECUTA EL INGRESO DE SESION CON CORREO POR DOCUMENTO
  }

  //INGRESA SESION DEL USUARIO
  Future<String> loginPostService(
      String usuario,
      String contrasena,
  ) async {
    try {
    var userlogin = await FirebaseAuth.instance.signInWithEmailAndPassword(email: usuario, password: contrasena);
      return userlogin.user.displayName;
    } catch (e) {
      print(e);
      return "error";
    }
  }
}
