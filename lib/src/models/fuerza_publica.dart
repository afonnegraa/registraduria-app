import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class FuerzaPublica {
  //CARGA EL LISTADO DE FUERZA PUBLICA 
  Future<List<DropdownMenuItem<String>>> getFuerzaPublicaService() async {
    List<DropdownMenuItem<String>> list = new List();
    List rawData = new List();
    QuerySnapshot data =  await Firestore.instance.collection('fuerzaPublica').getDocuments();
    data.documents.forEach((res) => {
      rawData.add({'data': res.data, 'id':res.documentID}) 
    });

    rawData.forEach((res) => {
      list.add(DropdownMenuItem(child: Text('${res['data']['nombreFuerza']}'), value: res['id']))
    });
    return list;
  }

  //CARGA EL LISTADO DE LOS RANGOS DE LA FUERZA PUBLICA SELECCIONADA
  Future<List<DropdownMenuItem<String>>> getCargoFuerzaPublicaService(id) async {
    List<DropdownMenuItem<String>> list = new List();
    List rawData = new List();
    QuerySnapshot data =  await Firestore.instance.collection('fuerzaPublica').document(id).collection('rangos').getDocuments();
    data.documents.forEach((res) => {
      rawData.add({'data': res.data, 'id':res.documentID}) 
    });

    rawData.forEach((res) => {
      list.add(DropdownMenuItem(child: Text('${res['data']['nombreRango']}'), value: res['id']))
    });
    return list;
  }

}