import 'package:flutter/material.dart';
import 'package:registraduria/src/services/barcode_service.dart';
import 'package:registraduria/src/global_components/dialog_component.dart'
    as dialog;
import 'package:registraduria/src/services/cedula_data_service.dart';

//CLASE PRINCIPAL QUE SOSTIENE UNA VISTA DE ESTADOS
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

//CLASE EN LA QUE LA VISTA Y SUS METODOS LO CONFORMAN
class _HomePageState extends State<HomePage> {
  //VARIABLES INICIALES Y DE USO
  final barcodeService = new BarcodeService();
  final estilosTexto = TextStyle(fontSize: 18.0, color: Colors.white);
  final estilosTextoResult = TextStyle(fontSize: 14.0, color: Colors.white,);
  final decoracionContenedor = BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        boxShadow: [
          BoxShadow(
              blurRadius: 0,
              color: Colors.black38,
              offset: Offset(0, 0),
              spreadRadius: 1.0)
        ]);
  String _identificacion;
  var _infoIdentificacion;
  bool _loader = false;

  //VISTA DE LA PÁGINA CON WIDGETS O COMPONENTES VISUALES INTEGRADOS EN FLUTTER
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.cyan,
        elevation: 0.0,
        title: Text('LECTOR DE CÉDULAS',
            style: TextStyle(
                color: Colors.white,
                fontSize: 30.0,
                fontWeight: FontWeight.bold)),
      ),
      body:  CustomScrollView(slivers: <Widget>[
              SliverList(
                delegate: SliverChildListDelegate([
                  _bodyPage(),
                ])
              )
      ]),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.cyan,
        foregroundColor: Colors.white,
        onPressed: () => _openCamera(context),
        child: Icon(Icons.photo_camera),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  //VISTA DEL BODY O CUERPO DE LA PAGINA
  Widget _bodyPage() {
    return  Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 20.0,
            ),
            _contenedor(),
            SizedBox(
              height: 20.0,
            ),
            _resultadoCedula(),
            
          ]),
    );
  }

  //VISTA DE CONTENEDOR DE INFORMACION DE COMO TOMAR FOTO
  Widget _contenedor() {
    return Container(
        width: double.infinity,
        padding: EdgeInsets.all(20.0),
        decoration: decoracionContenedor,
        child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
              Text(
                  '1. Revise que la cédula no este expuesta en un lugar muy luminoso.',
                  style: estilosTexto),
              SizedBox(
                height: 10.0,
              ),
              Text('2. No tape el código de barras de la cédula con el dedo.',
                  style: estilosTexto),
              SizedBox(
                height: 10.0,
              ),
              Text(
                  '3. Verifique que el código de la cédula se encuentre dentro del margen de la cámara.',
                  style: estilosTexto),
              SizedBox(
                height: 10.0,
              ),
              Text('4. Intente que la foto no quede borrosa.',
                  style: estilosTexto),
            ])));
  }

  //VISTA DE RESULTADO DE CEDULA
  Widget _resultadoCedula() {
    //SI EXISTE UNA CEDULA RETORNE LA INFORMACION
    if(_infoIdentificacion != null && _loader == false) {
      return Container(
        width: double.infinity,
          padding: EdgeInsets.all(20.0),
          decoration: decoracionContenedor,
                child: ListBody(
                  mainAxis: Axis.vertical,
                  children: <Widget>[
                    ListTile(
                      dense: true,
                      title: Container(
                        decoration: BoxDecoration( color: Colors.blue, borderRadius: BorderRadius.all(Radius.circular(5.0))),
                        height: 30.0,
                        child:Center(
                          child: Text('CERRAR', 
                            style: TextStyle(
                              color: Colors.white,
                            )
                          )
                        )
                      ),
                      onTap: () => {
                        setState(() {
                          _infoIdentificacion = null;
                        })
                      },
                    ),
                    ListTile(
                      title: Text('Reportado', style: estilosTexto),
                      subtitle:  _infoIdentificacion['reportado'] == true ? Text('Si', style: estilosTextoResult): Text('No', style: estilosTextoResult),
                    ),
                    ListTile(
                      title: Text('Identificación', style: estilosTexto),
                      subtitle: Text(_infoIdentificacion['identificacion'], style: estilosTextoResult),
                    ),
                    ListTile(
                      title: Text('Nombres', style: estilosTexto),
                      subtitle: Text(_infoIdentificacion['nombres'], style: estilosTextoResult),
                    ),
                    ListTile(
                      title: Text('Apellidos', style: estilosTexto),
                      subtitle: Text(_infoIdentificacion['apellidos'], style: estilosTextoResult),
                    ),
                    ListTile(
                      title: Text('Fecha de nacimiento', style: estilosTexto),
                      subtitle: Text(_infoIdentificacion['fechaNacimiento'], style: estilosTextoResult),
                    ),
                    ListTile(
                      title: Text('Lugar de nacimiento', style: estilosTexto),
                      subtitle: Text(_infoIdentificacion['lugarNacimiento'], style: estilosTextoResult),
                    ),
                    ListTile(
                      title: Text('Fecha de expedición', style: estilosTexto),
                      subtitle: Text(_infoIdentificacion['fechaExpedicion'], style: estilosTextoResult),
                    ),
                    ListTile(
                      title: Text('Lugar de expedición', style: estilosTexto),
                      subtitle: Text(_infoIdentificacion['lugarExpedicion'], style: estilosTextoResult),
                    ),
                    ListTile(
                      title: Text('Grupo sanguineo', style: estilosTexto),
                      subtitle: Text(_infoIdentificacion['grupoSanguineo'], style: estilosTextoResult),
                    ),
                    ListTile(
                      title: Text('RH', style: estilosTexto),
                      subtitle: Text(_infoIdentificacion['rH'], style: estilosTextoResult),
                    ),
                    ListTile(
                      title: Text('Estatura', style: estilosTexto),
                      subtitle: Text(_infoIdentificacion['estatura'], style: estilosTextoResult),
                    ),
                  ],
                ),
              );
    } else {
      //SI NO EXISTE RETORNE UN COMPOENENTE VACIO CON LA HABILIDAD DE MOSTRAR EL CARGADOR
      return Container(
        width: double.infinity,
          padding: EdgeInsets.all(20.0),
                child: _loader == true
                    ? Center(child: CircularProgressIndicator()) : null,
              );
    }
  }

  //METODO PARA ABRIR LA CAMARA Y RECONOCER LA CEDULA
  Future<void> _openCamera(context) async {
    setState(() {
      _loader = true;
    });
    _identificacion = await barcodeService.openCamera();
    if (_identificacion == null || _identificacion == "error") {
      _identificacion = null;
      dialog.errorDialog(
          'No se puede leer la cédula, intente nuevamente', context);
          setState(() {_loader = false;});
    } else {
      setState(() {});
      _identificacioninformation(context); 
    }
  }

  //METODO PARA CONSULTAR LA INFORMACION DEL CIUDADANO
  Future<void> _identificacioninformation(context) async {
    dynamic result = await CedulaDataService().postCedula(_identificacion);
    switch(result) {
      case 'NO_LOGEADO': 
        dialog.errorDialog('Este servicio no está disponible', context);
         _infoIdentificacion = null;
        break;
      case 'NO_EXISTE':
        dialog.errorDialog('La información del ciudadano no existe en la base de datos', context);
        _infoIdentificacion = null;
        break;
      case 'ERROR':
        dialog.errorDialog('Hubo un error, vuelve a intentar', context);
        _infoIdentificacion = null;
        break;
      default:
         _infoIdentificacion = result;
        break;
    }
    setState(() {_loader = false;});
  }

}
