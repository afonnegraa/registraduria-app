import 'package:flutter/material.dart';
import 'package:registraduria/src/models/fuerza_publica.dart';
import 'package:registraduria/src/global_components/dialog_component.dart'
    as dialog;
import 'package:registraduria/src/services/register_post_service.dart';

//CLASE PRINCIPAL QUE SOSTIENE UNA VISTA DE ESTADOS
class RegistrationPage extends StatefulWidget {
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

//CLASE EN LA QUE LA VISTA Y SUS METODOS LO CONFORMAN
class _RegistrationPageState extends State<RegistrationPage> {
  //VARIABLES INICIALES Y DE USO
  final _formKey = GlobalKey<FormState>();
  List<DropdownMenuItem<String>> _itemsFuerzaPublica =
      new List<DropdownMenuItem<String>>();
  List<DropdownMenuItem<String>> _itemsCargoFuerzaPublica =
      new List<DropdownMenuItem<String>>();
  String _idFuerza;
  String _idRango;
  String _identificacion;
  String _nombres;
  String _apellidos;
  String _correo;
  String _idFuncionario;
  bool _loading = false;

  //CONSTRUCTOR CON EJECUCION PARA
  //CONSULTA DEL LISTADO DE FUERZA PUBLICA
  _RegistrationPageState() {
    _getFuerzaPublicaData();
  }

  //VISTA DE LA PÁGINA CON WIDGETS O COMPONENTES VISUALES INTEGRADOS EN FLUTTER
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.cyan,
        elevation: 0.0,
        title: Text('REGISTRO',
            style: TextStyle(
                color: Colors.white,
                fontSize: 30.0,
                fontWeight: FontWeight.bold)),
      ),
      body: _body(context),
    );
  }

  //VISTA DEL BODY O CUERPO DE LA PAGINA
  Widget _body(context) {
    return SafeArea(
        child: Container(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
      child: CustomScrollView(slivers: <Widget>[
        SliverList(
          delegate: SliverChildListDelegate([
            SizedBox(
              height: 30.0,
            ),
            _form(context),
            SizedBox(
              height: 30.0,
            ),
          ]),
        )
      ]),
    ));
  }

  //FORMULARIO DE REGISTRO
  Widget _form(context) {
    //ESTILOS DEL CONTENEDOR
    final decoracionContenedor = BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        boxShadow: [
          BoxShadow(
              blurRadius: 0,
              color: Colors.black38,
              offset: Offset(0, 0),
              spreadRadius: 1.0)
        ]);
        //FORMULARIO VISUAL
    return Form(
        key: _formKey,
        child: Container(
            padding: EdgeInsets.all(20.0),
            decoration: decoracionContenedor,
            child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                //FORMULARIO INI
                children: <Widget>[
                  //CAMPOS DE LOGIN
                  TextFormField(
                    onChanged: (String valor) => {
                      setState(() {
                      _identificacion = valor;
                      })
                    },
                    keyboardType: TextInputType.phone,
                    autofocus: false,
                    decoration: _inputStyle('Identificación'),
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      Pattern pattern = r'^([0-9]{6,10})$';
                      RegExp regex = new RegExp(pattern);
                      if (value.isEmpty) {
                        return 'El campo no puede estar vacio';
                      }
                      if (!regex.hasMatch(value)){
                        return 'Solo números entre 6 a 10 carácteres';
                      } else {
                        return null;
                      }
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    onChanged: (String valor) => {
                      setState(() {
                      _nombres = valor;
                      })
                    },
                    autofocus: false,
                    decoration: _inputStyle('Nombres'),
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'El campo no puede estar vacio';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    onChanged: (String valor) => {
                      setState(() {
                      _apellidos = valor;
                      })
                    },
                    autofocus: false,
                    decoration: _inputStyle('Apellidos'),
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'El campo no puede estar vacio';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  DropdownButtonFormField(
                    decoration: _inputStyle('Fuerza pública'),
                    isDense: true,
                    iconEnabledColor: Colors.white,
                    style: TextStyle(color: Colors.cyan, fontSize: 16),
                    value: _idFuerza,
                    items: _itemsFuerzaPublica,
                    validator: (value) {
                      if (value == null) {
                        return 'El campo no puede estar vacio';
                      }
                      return null;
                    },
                    onChanged: (opt) {
                      setState(() {
                        _idRango = null;
                        _idFuerza = opt;
                        print(_idFuerza);
                        _getCargoFuerzaPublicaData();
                      });
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  DropdownButtonFormField(
                    decoration: _inputStyle('Cargo'),
                    isDense: true,
                    iconEnabledColor: Colors.white,
                    style: TextStyle(color: Colors.cyan, fontSize: 16),
                    value: _idRango,
                    items: _itemsCargoFuerzaPublica,
                    validator: (value) {
                      if (value == null) {
                        return 'El campo no puede estar vacio';
                      }
                      return null;
                    },
                    onChanged: (opt) {
                      setState(() {
                        _idRango = opt;
                        print(_idRango);
                      });
                    },
                  ),
                   SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    onChanged: (String valor) => {
                      setState(() {
                      _idFuncionario = valor;
                      })
                    },
                    keyboardType: TextInputType.phone,
                    autofocus: false,
                    decoration: _inputStyle('Identificación fuerza pública'),
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      Pattern pattern = r'^([0-9]{6,10})$';
                      RegExp regex = new RegExp(pattern);
                      if (value.isEmpty) {
                        return 'El campo no puede estar vacio';
                      }
                      if (!regex.hasMatch(value)){
                        return 'Solo números entre 6 a 10 carácteres';
                      } else {
                        return null;
                      }
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    onChanged: (String valor) => {
                      setState(() {
                      _correo = valor;
                      })
                    },
                    keyboardType: TextInputType.emailAddress,
                    autofocus: false,
                    decoration: _inputStyle('Correo'),
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                      RegExp regex = new RegExp(pattern);
                      if (value.isEmpty) {
                        return 'El campo no puede estar vacio';
                      }
                      if (!regex.hasMatch(value)){
                        return 'El correo no es válido';
                      } else {
                        return null;
                      }
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  SizedBox( //BOTON DE REGISTRO
                      width: double.infinity,
                      height: 50.0,
                      child: FlatButton(
                          textColor: Colors.white,
                          color: Colors.cyan,
                          disabledColor: Colors.blueGrey,
                          onPressed: _loading ? null : () => {
                                if (_formKey.currentState.validate()) {
                                  _loading = true,
                                  setState(() {}),
                                  _postRegistro(context)
                                }
                              },
                          child: _loading == false ? Text('REGISTRARSE') : Center(child: CircularProgressIndicator(
                            backgroundColor: Colors.white
                          ))
                          )
                        ),
                ]
                // FIN FORMULARIO
                )));
  }


  //ESTILOS DE LOS CAMPOS DE TEXTO
  _inputStyle(String placeholder) {
    return InputDecoration(
      labelText: placeholder,
      labelStyle: TextStyle(color: Colors.white),
      focusColor: Colors.white,
      hoverColor: Colors.white,
      fillColor: Colors.white,
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white70, width: 1.0),
      ),
      border: new OutlineInputBorder(
        borderRadius: const BorderRadius.all(
          const Radius.circular(10.0),
        ),
      ),
    );
  }

  //METODO PARA CONSULTAR LISTA DE FUERZA PUBLICA
  Future<void> _getFuerzaPublicaData() async {
    _itemsFuerzaPublica = await FuerzaPublica().getFuerzaPublicaService(); //SERVICIO DE EJECUCION
    if (_itemsFuerzaPublica == null) {
      dialog.errorDialog('No cargaron los datos de la fuerza pública', context);
    }
    setState(() {});
  }

  //METODO PARA CONSULTAR LISTA DE RANGOS DE FUERZA PUBLICA
  Future<void> _getCargoFuerzaPublicaData() async {
    _itemsCargoFuerzaPublica =
        await FuerzaPublica().getCargoFuerzaPublicaService(_idFuerza); //SERVICIO DE EJECUCION
    if (_itemsCargoFuerzaPublica == null) {
      dialog.errorDialog('No cargaron los datos de la fuerza pública', context);
    }
    setState(() {});
  }

  //METODO PARA EL POST DE REGISTRO
   Future<void> _postRegistro(context) async {
    String registro = await RegisterPostService().postRegistro(_idFuerza,_idRango,_identificacion,_nombres,_apellidos,_correo,_idFuncionario);
    _loading = false;
    switch(registro) {
      case 'NO_EXISTE': 
        dialog.errorDialog('El usuario no existe en la base de datos del Departamento de defensa', context);
        break;
      case 'EXISTE_SIN_RELACION':
        dialog.errorDialog('"El funcionario existe pero los datos del registro son erróneos', context);
        break;
      case 'USUARIO_REGISTRADO':
        dialog.errorDialog('El funcionario ya se encuentra registrado', context);
        break;
      case 'REGISTRO_EXITOSO':
        dialog.successDialog('Registro exitoso, revise su correo', context).then((d) => {
          Navigator.pushNamed(context, 'login') //CAMBIO DE VISTA
        });
        break;
      default: 
        dialog.errorDialog('Error de servidor', context);
        break;
    }
    setState(() {_loading = false;});
  }
}
