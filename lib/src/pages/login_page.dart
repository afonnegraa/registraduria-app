import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:registraduria/src/providers/db_provider.dart';
import 'package:registraduria/src/models/credenciales.dart';
import 'package:registraduria/src/global_components/dialog_component.dart'
    as dialog;

//CLASE PRINCIPAL QUE SOSTIENE UNA VISTA DE ESTADOS
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

//CLASE EN LA QUE LA VISTA Y SUS METODOS LO CONFORMAN
class _LoginPageState extends State<LoginPage> {
  //VARIABLES INICIALES Y DE USO
  final _formKey = GlobalKey<FormState>();
  TextEditingController _controllerContrasena;
  TextEditingController _controllerUsuario;
  String _usuario;
  String _contrasena;
  bool _recordar = true;
  bool _loading = false;
  dynamic _userData;

  //SI TIENE DATOS DE CONTRASEÑA CARGARLOS
  @override
  void initState() {
    super.initState();
    _getPassword();
  }

  //VISTA DE LA PÁGINA CON WIDGETS O COMPONENTES VISUALES INTEGRADOS EN FLUTTER
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverList(
            delegate: SliverChildListDelegate([
          _body(context),
        ]))
      ]),
    );
  }

  //VISTA DEL BODY O CUERPO DE LA PAGINA
  Widget _body(BuildContext context) {
    return SafeArea(
        child: Container(
      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
      height: MediaQuery.of(context).size.height - 30,
      width: double.infinity,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image(
                width: double.infinity,
                height: 250,
                image: AssetImage('assets/logo.png')),
            _form(context),
          ]),
    ));
  }

  //FORMULARIO DE LOGIN
  Widget _form(BuildContext context) {
    //ESTILOS DEL CONTENEDOR
    final decoracionContenedor = BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        boxShadow: [
          BoxShadow(
              blurRadius: 0,
              color: Colors.black38,
              offset: Offset(0, 0),
              spreadRadius: 1.0)
        ]);
        //FORMULARIO VISUAL
    return Form(
        key: _formKey,
        child: Container(
            padding: EdgeInsets.all(20.0),
            decoration: decoracionContenedor,
            child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                //FORMULARIO INI
                children: <Widget>[
                  //CAMPOS DE LOGIN
                  TextFormField(
                    controller: _controllerUsuario,
                    onChanged: (String valor) => {
                      setState(() {
                        print('entro');
                        _usuario = valor;
                      })
                    },
                    autocorrect: false,
                    autofocus: false,
                    decoration: _inputStyle('Identificación'),
                    keyboardType: TextInputType.phone,
                    style: TextStyle(color: Colors.white),
                    
                    validator: (value) {
                      Pattern pattern = r'^([0-9]{6,10})$';
                      RegExp regex = new RegExp(pattern);
                      if (value.isEmpty) {
                        return 'El campo no puede estar vacio';
                      }
                      if (!regex.hasMatch(value)) {
                        return 'Solo números entre 6 a 10 carácteres';
                      } else {
                        return null;
                      }
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    controller: _controllerContrasena,
                    onChanged: (String valor) => {
                      setState(() {
                        _contrasena = valor;
                      })
                    },
                    obscureText: true,
                    decoration: _inputStyle('Contraseña'),
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'El campo no puede estar vacio';
                      }
                      return null;
                    },
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Theme(
                          data: ThemeData(unselectedWidgetColor: Colors.white),
                          child: Checkbox(
                              value: _recordar,
                              checkColor: Colors.white,
                              activeColor: Colors.cyan,
                              onChanged: (bool newValue) {
                                setState(() {
                                  _recordar = newValue;
                                });
                              })),
                      Text('Recordar contraseña',
                          style: TextStyle(color: Colors.white))
                    ],
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  SizedBox(
                      width: double.infinity,
                      height: 50.0,
                      child: FlatButton(
                          textColor: Colors.white,
                          color: Colors.cyan,
                          disabledColor: Colors.blueGrey,
                          onPressed: _loading
                              ? null
                              : () => {
                                    //SI LOS CAMPOS ESTAN VALIDADOS CORRECTAMENTE
                                    //CONSULTAR SERVICIO DE LOGIN
                                    if (_formKey.currentState.validate())
                                      {
                                        _loading = true,
                                        setState(() {}),
                                        _postLogin(context)
                                      }
                                  },
                          child: _loading == false
                              ? Text('INGRESAR')
                              : Center(
                                  child: CircularProgressIndicator(
                                      backgroundColor: Colors.white)))), // BOTON DE INGRESAR
                  SizedBox(
                    height: 20.0,
                  ),
                  SizedBox(
                      width: double.infinity,
                      height: 30.0,
                      child: FlatButton(
                          textColor: Colors.white,
                          onPressed: () =>
                              {Navigator.pushNamed(context, 'register')},
                          child: Text('REGISTRARSE'))) //BOTON DE REGISTRO
                ]
                // FIN FORMULARIO
                )));
  }

  //ESTILOS DE LOS CAMPOS DE TEXTO
  _inputStyle(String placeholder) {
    return InputDecoration(
      labelText: placeholder,
      labelStyle: TextStyle(color: Colors.white),
      focusColor: Colors.white,
      hoverColor: Colors.white,
      fillColor: Colors.white,
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.white70, width: 1.0),
      ),
      border: new OutlineInputBorder(
        borderRadius: const BorderRadius.all(
          const Radius.circular(10.0),
        ),
      ),
    );
  }

  //METODO PARA LOGIN
  Future<void> _postLogin(context) async {
    //SI SE DESEA RECORDAR LA CONTRASEÑA
    if (_recordar) {
      await _setPassword();
    } else {
      //SI NO SE DESEA RECORDAR CONTRASEÑA
      await _deletePassword();
    }
    //EJECUCION DE LOGIN
    String login = await Credenciales().loginStart(_usuario, _contrasena);  //SERVICIO DE EJECUCION
    switch (login) {
      case "error":
        dialog.errorDialog(
            'Error de autenticación, verifica la identificación o la contraseña',
            context);
        break;
      default:
        Navigator.pushNamed(context, 'home'); //CAMBIO DE VISTA
        break;
    }
    //CUANDO TERMINAR CARGAR QUITAR CARGADOR
    setState(() {
      _loading = false;
    });
  }

  //CARGAR CONTRASEÑA GUARDADA
  Future<void> _getPassword() async {
    //CONSULTA DE DATOS LOCALES GUARADOS Y SE PONEN EN LOS CAMPOS
    _userData = await DBProvider.db.getPassword();
    _usuario = _userData['user'];
    _contrasena = _userData['password'];
    _controllerContrasena =
        new TextEditingController(text: _userData['password']);
    _controllerUsuario = new TextEditingController(text: _userData['user']);
    setState(() {
      print(_usuario);
    });
  }

  //ELIMINAR CONTRASEÑA GUARDADA LOCAL
  _deletePassword() async {
    await DBProvider.db.deletePassword();
  }

  //GUARDAR CONTRASEÑA LOCAL
  _setPassword() async {
    await DBProvider.db.savePassword(_usuario, _contrasena);
  }
}
