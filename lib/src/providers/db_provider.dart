import 'dart:io';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
/*
EL DATABASE PROVIDER ES UNA BASE DE DATOS LOCAL
CON EL UNICO USO DE GUARDAR CONTRASEÑA
*/
class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._private();

  DBProvider._private();

  //TRAER BASE DE DATOS LOCAL O CREARLA
  Future<Database> get database async {
    if (_database != null) {
      return _database;
    } else {
      _database = await initDB();
    }
    return _database;
  }

  //CREAR BASE DE DATOS Y TABLA USER
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final String path = join(documentsDirectory.path, 'ScansDB.db');
    return await openDatabase(path,
        version:
            1, //Cuando hay que hacer cambios respectivos si la base de datos existe
        onOpen: (db) {}, onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE user ('
          'id INTEGER PRIMARY KEY,'
          'user TEXT,'
          'password TEXT'
          ')');
    });
  }

  //CREAR REGISTROS EN DB
  Future savePassword(String user, String password) async {
    final db = await database;
    await deletePassword();
    final res = await db.insert('user', <String, dynamic> {
      'id': 1,
      'user': user,
      'password': password
    });
    print(res);
    return res;
  }

  //OBTENER REGISTROS EN DB
  Future<dynamic> getPassword() async {
    final db = await database;
    final res = await db.query('user', where: 'id = ?', whereArgs: [1]);
    return res.isNotEmpty ? res.first : null;
  }

  //ELIMINAR REGISTROS EN DB
  Future<dynamic> deletePassword() async {
    try {
    final db = await database;
    await db.delete('user', where: 'id = ?', whereArgs: [1]);
    } catch (error) {
      print(error);
    }
  }
}
