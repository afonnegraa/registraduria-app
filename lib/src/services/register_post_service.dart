import 'package:cloud_functions/cloud_functions.dart';

class RegisterPostService {
  //SERVICIO DE REGISTRO
  Future<String> postRegistro(
      String idFuerza,
      String idRango,
      String identificacion,
      String nombres,
      String apellidos,
      String correo,
      String idFuncionario
  ) async {
    //ENVIO DE DATOS PARA EL REGISTRO DEL FUNCIONARIO PUBLICO
    final HttpsCallable callable = CloudFunctions.instance
        .getHttpsCallable(functionName: 'registroService')
          ..timeout = const Duration(seconds: 60);
    try {
      final HttpsCallableResult result = await callable.call(
        <String, dynamic> {
          'idFuerza': idFuerza,
          'idRango': idRango,
          'identificacion': identificacion,
          'nombres': nombres,
          'apellidos': apellidos,
          'correo': correo,
          'idFuncionario': idFuncionario
        },
      );
      //SI EXITOSO RETONA DATOS
      return result.data;
    } on CloudFunctionsException catch (e) {
      print(e);
      return 'fail';
    } catch (e) {
      print(e);
      return 'fail';
    }
  }
}
