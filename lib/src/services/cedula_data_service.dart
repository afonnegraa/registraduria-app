import 'package:cloud_functions/cloud_functions.dart';

class CedulaDataService {
  //CONSULTAR EN LA BASE DE DATOS DE RESIGTRADURIA LA INFORMAICON DEL CIUDADANO CON LA CEDULA
  Future<dynamic> postCedula(
      String identificacion) async {
    final HttpsCallable callable = CloudFunctions.instance
        .getHttpsCallable(functionName: 'lecturaDocumentoService')
          ..timeout = const Duration(seconds: 30);
    try {
      final HttpsCallableResult result = await callable.call(
        <String, dynamic>{
          'identificacion': identificacion
        },
      );
      print(result.data);
      return result.data;
    } on CloudFunctionsException catch (e) {
      print(e);
      return 'ERROR';
    } catch (e) {
      print(e);
      return 'ERROR';
    }
  }
}
