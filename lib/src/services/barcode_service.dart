import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:image_picker/image_picker.dart';

class BarcodeService {
  String code = '';
  bool valid = true;
  //CONSTRUCTOR
  BarcodeService({this.code});

  //ABRIR CAMARA
  Future<String> openCamera() async {
    try {
      List<String> dataArray;
      final imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
      final image = FirebaseVisionImage.fromFile(imageFile);
      //PLUGIN PARA LEER CODIGO DE BARRAS
      final ccCodeDetector = FirebaseVision.instance.barcodeDetector(
          BarcodeDetectorOptions(barcodeFormats: BarcodeFormat.pdf417));
      //ALGORITMO DE LIMPIZA
      await ccCodeDetector.detectInImage(image).then((barcodes) {
        for (var item in barcodes) {
          var data = item.rawValue.replaceAll(new RegExp(r'[^\x00-\x7F]'), '|');
          dataArray = new List<String>.from(data.split('|'));
          dataArray
              .removeWhere((item) => item == '' || item == ' ' || item == null);
        }
      });

      code = _getCedula(dataArray);
      return code;
    } catch(e) {
      return 'error';
    }

  }
 
   //RECONOCER CEDULA Y DEVOLVERLA
   String _getCedula(data) {
    if (data == null) {
      valid = false;
      return null;
    }
    RegExp regex = new RegExp(r'[0-9][A-Za-z-0-9]');
    String cedula = data[3];
    if (!regex.hasMatch(cedula)) {
      cedula = data[2];
      if (cedula.substring(0, 1) == '00') {
        cedula = cedula.substring(0, 1);
      }
    }
    cedula = cedula.replaceAll(new RegExp(r'[A-Z]+'), '');
    if (cedula.length > 10) {
      cedula = cedula.substring(cedula.length - 10);
    }
    return cedula;
  }
}
