import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:registraduria/src/pages/home_page.dart';
import 'package:registraduria/src/pages/login_page.dart';
import 'package:registraduria/src/pages/register_page.dart';

//ESTRUCTURA PRINCIPAL PARA LA EJECUCION DE LA APLICACION
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark
        .copyWith(statusBarColor: Colors.white));
    return MaterialApp( // ESTILOS INICIALES
        debugShowCheckedModeBanner: false,
        title: 'Registraduria',
        initialRoute: 'login',
        themeMode: ThemeMode.dark,
        theme: ThemeData(
          errorColor: Colors.redAccent,
          scaffoldBackgroundColor: Colors.blueAccent,
          primaryColor: Colors.white,
          primaryColorDark: Colors.white,
          primarySwatch: Colors.lightBlue,
        ),
        routes: { //RUTAS VISUALES
          'home': (BuildContext context) => HomePage(),
          'login': (BuildContext context) => LoginPage(),
          'register': (BuildContext context) => RegistrationPage(),
        });
  }
}
